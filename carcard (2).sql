-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 23, 2020 at 10:35 PM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carcard`
--

-- --------------------------------------------------------

--
-- Table structure for table `categores`
--

CREATE TABLE `categores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categores`
--

INSERT INTO `categores` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(5, 'Information', '2020-11-13 19:22:00', '2020-11-13 19:35:59'),
(6, 'Vehicle servicing', '2020-11-17 13:09:21', '2020-11-17 13:09:21'),
(7, 'Window tinting', '2020-11-17 13:09:35', '2020-11-17 13:09:35'),
(8, 'Tyres', '2020-11-17 13:09:40', '2020-11-17 13:09:40'),
(9, 'Vehicle cleaning', '2020-11-17 13:09:50', '2020-11-17 13:09:50'),
(10, 'Driver Services', '2020-11-17 13:35:16', '2020-11-17 13:35:16');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `package_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `package_id`, `coupon_code`, `discount`, `discount_amount`, `created_at`, `updated_at`) VALUES
(1, '1', 'RMAMOTORS', '100', NULL, '2020-11-19 09:54:11', '2020-11-19 09:54:11');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_23_054839_create_posts_table', 1),
(5, '2020_10_23_112913_create_news_table', 1),
(6, '2020_11_05_082121_create_pakage_plans_table', 1),
(7, '2020_11_09_062838_create_vechile_records_table', 1),
(8, '2020_11_10_114823_create_coupons_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `sub_title`, `image`, `created_at`, `updated_at`) VALUES
(1, 'abc update', 'gjygub update', 'logo.png', '2020-11-09 09:12:03', '2020-11-12 01:44:59'),
(2, 'abc', 'gjygub', 'sport_complex.png', '2020-11-09 09:12:03', '2020-11-09 09:12:03'),
(3, 'sumsung mobile', 'gjygub', 'imgpsh_fullsize_anim (3).jpg', '2020-11-09 09:12:34', '2020-11-09 09:12:34'),
(4, 'aemal', 'FQWDEA', 'logo.png', '2020-11-12 01:47:35', '2020-11-12 01:47:35');

-- --------------------------------------------------------

--
-- Table structure for table `pakage_plans`
--

CREATE TABLE `pakage_plans` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `coupon_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pakage_plans`
--

INSERT INTO `pakage_plans` (`id`, `title`, `amount`, `duration`, `description`, `coupon_code`, `discount`, `discount_amount`, `created_at`, `updated_at`) VALUES
(1, 'Pay Monthly', '29', '31', 'Enjoy all the benefits of The Car Card but pay per month.', 'RMAMOTORS', '100', '0', '2020-11-17 12:36:58', '2020-11-19 09:54:11'),
(2, 'Pay annually', '299', '365', 'Enjoy all the benefits of The Car Card with one annual payment.', '1', '50', '149.5', '2020-11-17 13:05:17', '2020-11-18 01:04:38');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) UNSIGNED NOT NULL,
  `website_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `post_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `website_link`, `category_id`, `post_title`, `sub_title`, `description`, `image`, `user_id`, `created_at`, `updated_at`) VALUES
(14, 'www.aaadubai.com', 6, 'AAA Service Centre', 'Al Quoz 4', 'AAA Service Centre will offer The Car Card members the following deals\r\n\r\n10% discount on all services\r\nFree recovery and delivery within Dubai\r\nTerms and conditions apply.  Not applicable on tyres or any RTA charges and discount is only applicable on any invoice value over 1000 dirhams.  You must show your membership card to receive the deal.', '3AC41294-3A11-4D09-B800-C72A3B7DE42B.jpeg', NULL, '2020-11-17 13:17:47', '2020-11-17 13:17:47'),
(15, 'www.thedriver.ae', 10, 'The Driver', 'Driver Services', 'The Driver have agreed to provide the following offer for all The Car Card members :\r\n\r\n15% discount on all services.\r\nTerms and conditions apply and you are required to show your The Car Card membership to avail the offer.', 'C7AA288D-8F71-42CE-9EC3-AE4E0D13D9C1.jpeg', NULL, '2020-11-17 13:35:03', '2020-11-17 13:35:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `is_admin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pakage_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vechle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vechle_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vechle_year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateofbirth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expire_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `membership_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `is_admin`, `name`, `reset_token`, `pakage_id`, `email`, `phone`, `vechle_name`, `vechle_model`, `vechle_year`, `dateofbirth`, `address`, `password`, `expire_date`, `amount`, `status`, `membership_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(36, 'yes', 'Leon', NULL, NULL, 'leon@thecarcard.com', '0585871000', NULL, NULL, NULL, NULL, 'Dubai', '$2y$10$aV0GCZbDIQKRxP2lR5mJte0wZO2SV84LnA1yucsbxGQqTrB7imRCK', NULL, NULL, 'active', NULL, NULL, '2020-11-16 21:20:14', '2020-11-16 21:20:14'),
(38, 'no', 'abc', NULL, '5', 'abc@gmail.com', '03007718392', 'Audi', 'A3/S3', 'null', '11-2-2009', 'abs', '$2y$10$.ELRwli5QVgUZxQdDkQTbOHxr//QyFX24/LPBIxUVXcG12kYBPE.G', '2020-12-18 05:57:54', NULL, 'active', '88774299', NULL, '2020-11-17 12:57:54', '2020-11-17 12:57:54'),
(39, 'no', 'asad', NULL, '5', 'asadcsc24188@gmail.com', '03038975578', 'Audi', 'A3/S3', 'null', '21-11-2020', 'oook', '$2y$10$ASSzVdSlWituT/.9VQRot.keL1p1XnF8k9X07Bw0HK1OPyY13sNjC', '2020-12-18 06:02:28', NULL, 'active', '33983441', NULL, '2020-11-17 13:02:28', '2020-11-17 13:02:28'),
(40, 'no', 'ggs', NULL, '5', 'asad.mirchu@gmail.com', '8023868', 'Audi', 'A3/S3', 'null', '17-11-2020', 'uubb', '$2y$10$swovtrWQVIp8m0E41hpnc.4aPORsmHwGPRHXAZYwUz3FS20uq3f22', '2020-12-18 06:05:41', NULL, 'active', '62279736', NULL, '2020-11-17 13:05:41', '2020-11-17 13:05:41'),
(41, 'no', 'hzvzg', NULL, '5', 'asad24188@gmail.com', '6858383', 'Audi', 'A3/S3', 'null', '17-11-2022', 'uhh', '$2y$10$6i7LBQ3wp0R3MDMfPT7BIONG7jkYKhrLQ5EyUaqX/bao07pgCi3aa', '2020-12-18 06:08:08', NULL, 'active', '13474757', NULL, '2020-11-17 13:08:08', '2020-11-17 13:08:08'),
(42, 'no', 'gzgz', NULL, '5', 'asadasad@gmail.com', '55555555', 'Audi', 'A3/S3', 'null', '17-11-2020', 'dsd', '$2y$10$hshJJNVLeQGwUkTojekG9.yrR5reZedhSQnVTWuTYgQXAv08sXhsK', '2020-12-18 06:09:33', NULL, 'active', '81703235', NULL, '2020-11-17 13:09:33', '2020-11-17 13:09:33'),
(43, 'no', 'xhxhxh', NULL, '5', 'asadaliasad@gmail.com', '989898989', 'Audi', 'A3/S3', 'null', '17-11-2020', 'hxbxh', '$2y$10$7ID2WSCscWTXVkN3E8V34eF3HVzElyaZZ6h5X6khUbTKXhFlW7XYG', '2020-12-18 06:12:57', NULL, 'active', '27203980', NULL, '2020-11-17 13:12:57', '2020-11-17 13:12:57'),
(44, 'no', 'hxbxb', NULL, '5', 'asadcsc24bxb188@gmail.com', '98989', 'Audi', 'A3/S3', 'null', '17-11-2020', 'bxvxb', '$2y$10$tFC1T8B0exRQXBlLw2Y4K.83MlZowO4UEQ.JFwAOxQtFsI3N4HEKm', '2020-12-18 06:15:14', NULL, 'active', '27713161', NULL, '2020-11-17 13:15:14', '2020-11-17 13:15:14'),
(45, 'no', 'hzhzhz', NULL, '5', 'asadcsc24tzgz188@gmail.com', '676767', 'Audi', 'A3/S3', 'null', '17-11-2020', 'bxbxhx', '$2y$10$YC.djCQwbJbwA2c/A8eui.A0Hgr2PtVuLOMKxMyz3RxN/yvcmhGxK', '2020-12-18 06:20:50', NULL, 'active', '39186239', NULL, '2020-11-17 13:20:50', '2020-11-17 13:20:50'),
(46, 'no', 'abc', NULL, '5', 'abcd@gmail.com', '136794764', 'Audi', 'Cayman', 'null', '17-6-2015', 'fsjsks', '$2y$10$bKaTM3uxI.pXS9gS4n8u9.zGKi3E.r6OHTGJw5HssVSAOl2tFIXXW', '2020-12-18 06:26:17', NULL, 'active', '52821043', NULL, '2020-11-17 13:26:17', '2020-11-17 13:26:17'),
(47, 'no', 'gdksn', NULL, '5', 'dhl@gamail.con', '1379497694', 'BMW', 'Boxster', 'null', '17-11-2020', 'fshsjs', '$2y$10$IL8u7tRXT4AynYLfOXbpQeLEzbBPo/vurinrFpR9OPgVkR1t6Uq6K', '2020-12-18 06:27:49', NULL, 'active', '19130081', NULL, '2020-11-17 13:27:49', '2020-11-17 13:27:49'),
(48, 'no', 'vvxxvzv', NULL, '5', 'asadcscvxbx24188@gmail.com', '979797', 'Porsche', 'Cayman', 'null', '17-11-2020', 'vxvxxv', '$2y$10$kr34Qs6WMveYIyqTcgoWE.GY1vIY739KYp1PMo6gqrAQxhkUly8I.', '2020-12-18 06:38:35', NULL, 'active', '81499938', NULL, '2020-11-17 13:38:35', '2020-11-17 13:38:35'),
(49, 'no', 'Asad', NULL, '5', 'asadtest@gmail.com', '346454646', 'BMW', 'Cayenne', 'null', '17-11-2020', 'okara', '$2y$10$TXPjY9E4KUQ71McvI58f3.wtogCHWlyNiTA6tkYKWtygtxFQnJFcO', '2020-12-18 06:44:54', NULL, 'active', '81310996', NULL, '2020-11-17 13:44:54', '2020-11-17 13:44:54'),
(50, 'no', 'text', '9735', '5', 'text', '1234', 'Vehicle Type', 'Vehicle Model', 'Vehicle Year', '16-11-1900', 'text', '$2y$10$kxMU1cVMqQy2qLp.D4Eg7OnhhXnXmNXVtgBq5cdtR1.xmhaX4LN0q', '2020-12-18 07:09:18', NULL, 'active', '50004587', NULL, '2020-11-17 14:09:18', '2020-11-17 14:11:02'),
(51, 'no', NULL, NULL, NULL, 'admin@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$pWsIAdbLWYsWoOvZ3lQQ8OI09o6hhHjOBrvVz2/0EZs6.UiKBjTSa', NULL, NULL, 'active', NULL, NULL, '2020-11-17 20:14:00', '2020-11-17 20:14:00'),
(52, 'yes', NULL, NULL, NULL, 'muhammadmunib12596@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$bitsnIiGprX23QpRNHZKtuQ/BV9VPuBGuWj8HQuBEPk0PUigogcYG', NULL, NULL, 'active', NULL, NULL, '2020-11-17 20:15:56', '2020-11-17 20:15:56'),
(53, 'no', 'John Doe', NULL, '1', 'playstorecnx2020@gmail.com', '63664434', 'BMW', 'Cayman', 'null', '21-11-1998', '15 parkland avenue', '$2y$10$kCwWWMPPrC.TPZ5rP476nOWfUf3lesZtiCXsUbSmbNsC8l3iDLUrq', '2020-12-22 04:55:43', '29', 'active', '88863987', NULL, '2020-11-21 11:55:43', '2020-11-21 11:55:43'),
(54, 'no', 'jannie doe', NULL, '1', 'boracay.gpt22021@gmail.com', '9772817698', 'Audi', 'A3/S3', 'null', '1-1-1990', NULL, '$2y$10$AyvJbTN7CH8dviSD//dQieACdbFDjOFd6M0asax2WOHoxozNZ/.ZG', '2020-12-22 13:26:51', '29', 'active', '73237652', NULL, '2020-11-21 20:26:51', '2020-11-21 20:26:51');

-- --------------------------------------------------------

--
-- Table structure for table `vechile_records`
--

CREATE TABLE `vechile_records` (
  `id` int(11) UNSIGNED NOT NULL,
  `vechile_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vechile_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vechile_records`
--

INSERT INTO `vechile_records` (`id`, `vechile_type`, `vechile_model`, `year`, `created_at`, `updated_at`) VALUES
(3, 'Audi', 'A3/S3', NULL, '2020-11-17 12:26:59', '2020-11-18 13:03:08'),
(4, 'Porsche', '911', NULL, '2020-11-17 13:24:09', '2020-11-17 13:24:09'),
(5, 'Porsche', 'Cayman', NULL, '2020-11-17 13:24:21', '2020-11-17 13:24:21'),
(6, 'Porsche', 'Boxster', NULL, '2020-11-17 13:24:31', '2020-11-17 13:24:31'),
(7, 'Porsche', 'Macan', NULL, '2020-11-17 13:24:40', '2020-11-17 13:24:40'),
(8, 'Porsche', 'Taycan', NULL, '2020-11-17 13:24:50', '2020-11-17 13:24:50'),
(9, 'Porsche', 'Cayenne', NULL, '2020-11-17 13:24:58', '2020-11-17 13:24:58'),
(11, 'Porsche', 'Panamerra', NULL, '2020-11-17 13:25:28', '2020-11-17 13:25:28'),
(12, 'Porsche', '968', NULL, '2020-11-17 13:25:39', '2020-11-17 13:25:39'),
(13, 'Porsche', '928', NULL, '2020-11-17 13:25:48', '2020-11-17 13:25:48'),
(14, 'Porsche', '356', NULL, '2020-11-17 13:25:57', '2020-11-17 13:25:57'),
(15, 'BMW', 'X5', NULL, '2020-11-17 13:26:04', '2020-11-17 13:26:04'),
(16, 'BMW', 'X3', NULL, '2020-11-17 13:26:16', '2020-11-17 13:26:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categores`
--
ALTER TABLE `categores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pakage_plans`
--
ALTER TABLE `pakage_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vechile_records`
--
ALTER TABLE `vechile_records`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categores`
--
ALTER TABLE `categores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pakage_plans`
--
ALTER TABLE `pakage_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `vechile_records`
--
ALTER TABLE `vechile_records`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
